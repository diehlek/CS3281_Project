Elizabeth Diehl
Clara Yip
Nachos

3.7 Nachos Machine

1. Using the config file given above, what is the number of physical pages in the main memory? 
	-1 pages. In Machine.java, the private static int numPhysPages is originally set to -1. In createDevices(), numPhysPages is only updated if Config.getBoolean(�Machine.processor�) is true, which it is not as per the conf file. Additionally, no new Processor object is created since Machine.processor = false in the conf file. 

2. Why is not any processor object created in this example? How can Nachos simulate a machine without any processor? 
       Machine.processor = false in the config file, so the machine does not provide a MIPS processor. Nachos can run in kernel mode. 
       
3. Provide a snapshot of the outputs from Nachos running the config file in Project 1. Compare the results when running it multiple times. Are they all the same? Why? 

The results for running the config file each time are all the same because the random number generator is set to generate the same numbers every execution. The option to set the random number generator seed is never utilized, so the default in Machine.java is randomSeed = 0, which never changes between executions. 
4.3 Threads and Scheduling
1. In Eclipse Debug mode, what are the values of the name and the status fields of the currentThread object? What about the idleThread object?
	currentThread: statusRunning
	idleThread: statusBlocked
2. Provide the output of the program with the modified source code.
	We don�t know how to run this source code. With the modifications, the threads� execution would be clearly traced, but KThread.java is not printing the output to the console, so the program is not outputting any further information. Furthermore, KThread.class in proj2 cannot be opened/edited without an application to read java class files. 
3. Modify KThread.selfTest() to 5 KThreads for PingTest and trace the order of execution(you can name the threads accordingly to see things more clearly).
a. Once again, we can�t view or change class file to do what this question is asking. 

5.5 User Level Process
1. What are the variables this.argc and this.argv? 
a. argc and argv are both private ints. argc is equal to the length of the given array args, which is a parameter for the arguments to be passed to the executable in load. this.argv is an int that stores the offset of an entry.
2. In Line 64 � 72, how exactly are the arguments stored? [Hints: not all arguments are of the same length.]
a. The arguments are stored in byte arrays.

1. How many sections are there in halt.coff? What are they? 
a. As it is without being able to open a .coff file, we cannot tell how many sections that are in the file, or what they are. 
2. How many pages do the sections occupy?
a. The amount of pages that the sections occupy is equal to however long the sections.
3. How many pages does the stack occupy?
a. According to the program in UserProcess.java, the stack will occupy 8 pages.
4. What are the values of the initial PC and SP? 
a. The initial PC points to the program entry point, so initialPC = coff.getEntryPoint()
b. The initial SP points to the top of the stack, which in nachos is numPages*pageSize, where numPages includes the number of pages are in the stack. 
5. Change the shell program to test/matmult.coff. Answer the above questions when Dim = 20 and Dim = 2000.
a. We�re not quite sure what this question is asking. Additionally, as it stands changing the shell program is not possible as these types of files cannot currently be opened to view or edit.

We did not install Eclipse to view/edit the nachos code, so most of the questions requiring debugging could not be done. Additionally, we met in person to go over the questions together.
